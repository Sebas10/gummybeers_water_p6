﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Gemaakt met behulp van https://www.youtube.com/watch?time_continue=11&v=3qBDTh9zWrQ

Shader "Custom/Toon" 
{
	Properties
	{
		m_Color("Diffuse Material Color", Color) = (1, 1, 1, 1)
		m_UnlitColor("Unlit Color", Color) = (0.5, 0.5, 0.5, 1)
		m_DiffuseThreshold("Lighting Threshold", Range(-1.1, 1)) = 0.1
		m_SpecColor("Specular Material Color", Color) = (1, 1, 1, 1)
		m_Shininess("Shininess", Range(0.5, 1)) = 1
		m_OutlineThickness("Outline Thickness", Range(0, 1)) = 0.1
		m_MainTex("Texture (RGB)", 2D) = "white" {}
	}

		SubShader
	{
		Pass
		{
			Tags{"LightMode" = "ForwardBase"}

			CGPROGRAM

			#pragma vertex vert

			#pragma fragment frag

			uniform float4 m_Color;
			uniform float4 m_UnlitColor;
			uniform float m_DiffuseThreshold;
			uniform float4 m_SpecColor;
			uniform float m_Shininess;
			uniform float m_OutlineThickness;

			uniform float4 m_LightColor0;

			struct vertexInput {

				float4 Vertex : POSITION;
				float3 Normal : NORMAL;
				float4 TexCoord : TEXCOORD0;

			};

			struct vertexOutput {

				float4 POS : SV_POSITION;
				float3 NormalDir : TEXCOORD1;
				float4 LightDir : TEXCOORD2;
				float3 ViewDir : TEXCOORD3;
				float2 UV : TEXCOORD0;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				output.NormalDir = normalize(mul(float4(input.Normal, 0.0), unity_WorldToObject).xyz);

				float4 posWorld = mul(unity_ObjectToWorld, input.Vertex);

				output.ViewDir = normalize(_WorldSpaceCameraPos.xyz - posWorld.xyz);

				float3 fragmentToLightSource = (_WorldSpaceCameraPos.xyz - posWorld.xyz);
				output.LightDir = float4(normalize(lerp(_WorldSpaceLightPos0.xyz, fragmentToLightSource, _WorldSpaceLightPos0.w)), lerp(1.0, 1.0 / length(fragmentToLightSource), _WorldSpaceLightPos0.w)
				);

				output.POS = UnityObjectToClipPos(input.Vertex);

				output.UV = input.TexCoord;

				return output;

			}

			float4 frag(vertexOutput input) : COLOR
			{

				float nDotL = saturate(dot(input.NormalDir, input.LightDir.xyz));

				float DiffuseCutoff = saturate((max(m_DiffuseThreshold, nDotL) - m_DiffuseThreshold) * 1000);

				float SpecularCutoff = saturate(max(m_Shininess, dot(reflect(-input.LightDir.xyz, input.NormalDir), input.ViewDir)) - m_Shininess) * 1000;

				float OutlineStrength = saturate((dot(input.NormalDir, input.ViewDir) - m_OutlineThickness) * 1000);


				float3 AmbientLight = (1 - DiffuseCutoff) * m_UnlitColor.xyz;
				float3 DiffuseReflection = (1 - SpecularCutoff) * m_Color.xyz * DiffuseCutoff;
				float3 SpecularReflection = m_SpecColor.xyz * SpecularCutoff;

				float3 CombinedLight = (AmbientLight + DiffuseReflection) * OutlineStrength + SpecularReflection;

				return float4(CombinedLight, 1.0);
			}
				ENDCG
		}
	}
		Fallback "Diffuse"
}
