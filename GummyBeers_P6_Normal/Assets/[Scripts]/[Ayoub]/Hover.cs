﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Hover : MonoBehaviour
{
    [SerializeField]
    private TextMesh m_Text;

    [SerializeField]
    private string m_Line;

    [SerializeField]
    private float m_XPos, m_YPos;

    private void Start()
    {
        m_Text.gameObject.SetActive(false);
    }

    private void OnMouseEnter()
    {
        m_Text.gameObject.SetActive(true);

        m_Text.text = m_Line;
    }

    private void OnMouseExit()
    {
        m_Text.gameObject.SetActive(false);
    }
}