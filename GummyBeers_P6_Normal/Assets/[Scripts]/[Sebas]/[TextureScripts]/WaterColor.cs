﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterColor : Percentage_Water
{
    public Renderer m_Rend;

    [SerializeField] private GameObject m_Afval;

    public int m_RoundedPercentage = 0;

    public Color m_StartColor;

    void Start()
    {
        m_Rend = GetComponent<Renderer>();

        m_Rend.material.color = m_StartColor;

        m_RoundedPercentage = 0;
    }

    void Update()
    {
        
        switch (m_RoundedPercentage)
        {
            case 10:
                m_Rend.material.SetColor("m_Color", new Color(0, 0, 255));
                break;
            case 9:
                m_Rend.material.SetColor("m_Color", new Color(100, 0, 255));
                break;
            case 8:
                m_Rend.material.SetColor("m_Color", new Color(75, 83, 0));
                break;
            case 7:
                m_Rend.material.SetColor("m_Color", new Color(255, 255, 0));
                break;
            case 6:
                m_Rend.material.SetColor("m_Color", new Color(100, 255, 100));
                break;
            case 5:
                m_Rend.material.SetColor("m_Color", new Color(0, 255, 0));
                break;
            case 4:
                m_Rend.material.SetColor("m_Color", new Color(255, 0, 0));
                break;
            case 3:
                LerpColor(0, 0, 0);
                break;
            case 2:
                LerpColor(0, 0, 0);//Start Color               
                break;
            case 1:                
                m_Afval.SetActive(false);//Afval verwijderd                
                break;
            case 0:
                m_Rend.material.color = m_StartColor;//Start Color zwart
                m_Afval.SetActive(true);                
                break;

            default:
                m_Rend.material.color = m_StartColor;
                break;
        }
    }

    public void ValuePlus(int num)
    {
        m_RoundedPercentage = num;
    }

    public void ValueMin(int num)
    {
        m_RoundedPercentage = num;
    }

    /// <summary>
    /// RGB colors set value of red, green and blue
    /// </summary>
    /// <param name="r">RED</param>
    /// <param name="g">GREEN</param>
    /// <param name="b">BLUE</param>
    private void LerpColor(float r, float g, float b)
    {
        m_Rend.material.SetColor("m_Color", new Color(r,g,b));
    }
}
