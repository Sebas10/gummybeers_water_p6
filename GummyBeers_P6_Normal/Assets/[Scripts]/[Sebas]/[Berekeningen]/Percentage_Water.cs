﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Percentage_Water : MonoBehaviour
{
    public Slider m_Schoonheid;

    void Start()
    {
        m_Schoonheid.value = 0;
    }

    public void ChangeValue()
    {
        m_Schoonheid.value += 1;

        if(m_Schoonheid.value == 10)
        {
            SceneManager.LoadScene(3);
        }
    }

    public void ResetValue(int value)
    {
        m_Schoonheid.value -= value;
    }
}
