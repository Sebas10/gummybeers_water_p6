﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{
    private Renderer m_Rend;
    private Color m_HoverColor;

    private int m_ButtonPercentage;
    private int num = 0;

    private int m_RightWay = 0;

    private bool[] m_ResetBool = new bool[10];

    private WaterColor m_WaterColor;
    private Percentage_Water m_PW;

    private void Start()
    {
        m_Rend = GetComponent<Renderer>();

        m_HoverColor = new Color(100, 100, 100);        

        m_WaterColor = GameObject.FindObjectOfType<WaterColor>();

        m_PW = FindObjectOfType<Percentage_Water>();

        if (m_WaterColor == null)
        {
            Debug.LogError("WaterColor object null!");
        }


        for (int i = 0; i < m_ResetBool.Length; i++)
        {
            m_ResetBool[i] = false;
        }
    }

    private void OnMouseOver()
    {
        Sections(gameObject.name, true, gameObject);
    }
    private void OnMouseExit()
    {
        Sections(gameObject.name, false, gameObject);
    }

    #region Functions Sections

    /// <summary>
    /// Section switch case to light objects on and off and use them.
    /// </summary>
    /// <param name="name">Object Name</param>
    /// <param name="state">On or off with the mouse</param>
    /// <param name="objectchild">Has it any childs then light them up as well</param>
    private void Sections(string name, bool state, GameObject objectchild)
    {
        Debug.Log(num);
        switch (name)
        {
            ////////////////////////////////////////////////////////////////////////////////SectionLeft

            case "Top_Left"://(1) Verwijder Afval
                if (state)
                {
                    m_Rend.material.color = m_HoverColor;

                    if (Input.GetMouseButtonDown(0))
                    {
                        m_ResetBool[0] = true;
                        StartCoroutine(Select());
                    }
                                  
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Left_Section_Top"://6
                if (state)
                {
                    m_ResetBool[5] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Left_Section_Middle"://2
                if (state)
                {
                    m_Rend.material.color = m_HoverColor;

                    if (Input.GetMouseButtonDown(0))
                    {
                        m_ResetBool[1] = true;
                        StartCoroutine(Select());
                    }
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Left_Section_Bottom"://7
                if (state)
                {
                    m_ResetBool[6] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            ////////////////////////////////////////////////////////////////////////////////SectionLeftMiddle

            case "Middle_Left_Section_Top"://(5)
                if (state)
                {
                    m_ResetBool[4] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Left_Section_Middle_Top":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Left_Section_Middle_Bottom":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Left_Section_Bottom":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            ////////////////////////////////////////////////////////////////////////////////SectionMiddleMiddle

            case "Middle_Middle_Top_Left"://9
                if (state)
                {
                    m_ResetBool[8] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Middle_Top_Right":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Middle_Bottom_Left":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Middle_Bottom_Middle"://10
                if (state)
                {
                    m_ResetBool[9] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Middle_Bottom_Right":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            ////////////////////////////////////////////////////////////////////////////////SectionBottomMiddle

            case "Bottom_Middle_Left":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Bottom_Middle_Center_Left"://3
                if (state)
                {
                    m_ResetBool[2] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Bottom_Middle_Center_Right":
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Bottom_Middle_Right"://(4)Aleminium sulvaal toevoegen
                if (state)
                {
                    m_ResetBool[3] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            ////////////////////////////////////////////////////////////////////////////////SectionRightMiddle

            case "Middle_Right_Section_Top"://(8)Antraciet en zout toevoegen
                if (state)
                {
                    m_ResetBool[7] = true;
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Right_Section_Middle_Top"://Fout
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Right_Section_Middle_Bottom"://Fout
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            case "Middle_Right_Section_Bottom"://Fout
                if (state)
                {
                    StartCoroutine(Select());
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            ////////////////////////////////////////////////////////////////////////////////SectionTopRight

            case "Top_Right":
                if (state)
                {
                    if (Input.GetMouseButtonDown(0))
                    {
                        StartCoroutine(Select());
                    }
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            ////////////////////////////////////////////////////////////////////////////////SectionBottomRight


            //Reset Button
            case "Bottom_Right":
                if (state)
                {
                    m_Rend.material.color = m_HoverColor;
                    if (Input.GetMouseButtonDown(0))
                    {
                        ResetValues();
                        m_PW.ResetValue(Mathf.RoundToInt(m_PW.m_Schoonheid.value));
                    }                        
                }
                else
                    m_Rend.material.color = Color.white;
                break;

            default:
                m_Rend.material.color = Color.white;
                break;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            num += 1;
            m_WaterColor.ValuePlus(num);
            m_PW.ChangeValue();
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetValues();
        }
    }
    #endregion

    private IEnumerator Select()
    {
        if (Input.GetMouseButtonDown(0))
        {
            //if(m_ResetBool[0])//(1) Afval
            //{
            //    num = 1;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1])
            //{
            //    num = 2;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1] && m_ResetBool[2])
            //{
            //    num = 3;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1] && m_ResetBool[2] && m_ResetBool[3])
            //{
            //    num = 4;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1] && m_ResetBool[2] && m_ResetBool[3] && m_ResetBool[4])
            //{
            //    num = 5;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1] && m_ResetBool[2] && m_ResetBool[3] && m_ResetBool[4] && m_ResetBool[5])
            //{
            //    num = 6;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1] && m_ResetBool[2] && m_ResetBool[3] && m_ResetBool[4] && m_ResetBool[5] && m_ResetBool[6])
            //{
            //    num = 7;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1] && m_ResetBool[2] && m_ResetBool[3] && m_ResetBool[4] && m_ResetBool[5] && m_ResetBool[6] && m_ResetBool[7])
            //{
            //    num = 8;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else if (m_ResetBool[0] && m_ResetBool[1] && m_ResetBool[2] && m_ResetBool[3] && m_ResetBool[4] && m_ResetBool[5] && m_ResetBool[6] && m_ResetBool[7] && m_ResetBool[8])
            //{
            //    num = 9;
            //    m_WaterColor.ValuePlus(num);
            //    m_PW.ChangeValue();
            //}
            //else
            //{
            //    ResetValues();
            //}
            m_Rend.material.color = Color.blue;
        }
        else
        {            
            m_Rend.material.color = m_HoverColor;
            yield return new WaitForSeconds(0.25f);
        }        
    }

    private void ResetValues()
    {
        SceneManager.LoadScene(2);
    }
    

}

