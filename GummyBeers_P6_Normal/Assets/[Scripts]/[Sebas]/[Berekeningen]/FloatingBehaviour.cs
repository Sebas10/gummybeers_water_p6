﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingBehaviour : MonoBehaviour
{
    private Vector3 worldPos;

    void Start()
    {
        worldPos = transform.position;
        GetComponent<Rigidbody>().isKinematic = true;
    }

    // Update is called once per frame
    void Update()
    {
        float m_Speed = 87f;
        float m_Strength = 0.2f;
        float displacement = (Mathf.Cos(worldPos.z) + Mathf.Cos(worldPos.x + (m_Speed * Time.time / 20.0f)));
        Vector3 offsetPosition = worldPos;
        offsetPosition.y += m_Strength * displacement;
        transform.position = offsetPosition;
    }
}
