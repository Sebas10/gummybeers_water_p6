﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Vector3 m_V3Rotate = Vector3.zero;
    private float m_MinVertical = -5;
    private float m_MaxVertical = 40;
    private float m_MinHorizontal = -45;
    private float m_MaxHorizontal = 45;
    private float m_RotationSpeed = 45f;


    void Start()
    {
        transform.localEulerAngles = m_V3Rotate;
    }

    void Update()
    {
        RotateCamera();
    }

    private void RotateCamera()
    {
        //Rotate vertical
        m_V3Rotate.x += Input.GetAxis("Mouse Y") * m_RotationSpeed * Time.deltaTime;
        m_V3Rotate.x = Mathf.Clamp(m_V3Rotate.x, m_MinVertical, m_MaxVertical);

        //Rotate horizontal
        m_V3Rotate.y += Input.GetAxis("Mouse X") * m_RotationSpeed * Time.deltaTime;
        m_V3Rotate.y = Mathf.Clamp(m_V3Rotate.y, m_MinHorizontal, m_MaxHorizontal);

        transform.localEulerAngles = m_V3Rotate;
    }
}

