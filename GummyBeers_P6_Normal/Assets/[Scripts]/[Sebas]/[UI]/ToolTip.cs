﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToolTip : MonoBehaviour
{
    [SerializeField]
    public string m_String;
    public string m_StringWhenStarted;

    [SerializeField]
    private GameObject m_Image;

    [SerializeField]
    public Text m_Text;

    private bool m_DisplayText;

    private void Start()
    {
        m_StringWhenStarted = m_String;
    }

    void Update()
    {
        DisplayText();
    }

    private void OnMouseOver()
    {
        m_DisplayText = true;
    }

    private void OnMouseExit()
    {
        m_DisplayText = false;
    }

    private void DisplayText()
    {
        if (m_DisplayText == true)
        {
            m_Image.SetActive(true);
            m_Text.text = m_String;
        }

        else
        {
            m_Image.SetActive(false);
            m_Text.text = "";
        }
    }
}
