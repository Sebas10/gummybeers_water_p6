﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Gemaakt met behulp van https://www.youtube.com/watch?time_continue=11&v=3qBDTh9zWrQ

Shader "Custom/Toon" {
	Properties
	{
		m_Color("Diffuse Material Color", Color) = (1, 1, 1, 1)
		m_UnlitColor("Unlit Color", Color) = (0.5, 0.5, 0.5, 1)
		m_DiffuseThreshold("Lighting Threshold", Range(-1.1, 1)) = 0.1
		m_SpecColor("Specu;ar Material Color", Color) = (1, 1, 1, 1)
		m_Shininess("Shininess", Range(0.5, 1)) = 1
		m_OutlineThickness("Outline Thickness", Range(0, 1)) = 0.1
		m_MainTex("Texture (RGB)", 2D) = "white" {}
	}

	SubShader
	{
		Pass
		{
			/*Tags{"LightMode" = "ForwardBase"}*/

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert

			// Use shader model 3.0 target, to get nicer looking lighting
			#pragma fragment frag
			
			// 
			uniform float4 m_Color;
			uniform float4 m_UnlitColor;
			uniform float m_DiffuseThreshold;
			uniform float4 m_SpecColor;
			uniform float m_Shininess;
			uniform float m_OutlineThickness;

			uniform float4 m_LightColor0;
			uniform sampler2D m_MaintTex;
			uniform float4 m_MainTex_ST;

			struct VertexInput 
			{
				float4 Vertex : POSITION;
				float3 Normal : NORMAL;
				float4 TextCoord : TEXCOORD0;
			};

			struct VertexOutput 
			{
				float4 pos : SV_POSITION;
				float3 NormalDir : TEXTCOORD1;
				float4 LightDir : TEXTCOORD2;
				float3 ViewDir : TEXTCOORD3;
				float2 UV : TEXTCOORD0;
			};

			VertexOutput vert(VertexInput input)
			{
				VertexOutput output;

				output.NormalDir = normalize(mul(float4(input.Normal, 0.0), unity_WorldToObject).xyz);

				float4 POSWorld = mul(unity_WorldToObject, input.Vertex);

				output.ViewDir = normalize(_WorldSpaceCameraPos.xyz - POSWorld.xyz);

				float3 FragmentToLightSource = (_WorldSpaceCameraPos.xyz - POSWorld.xyz);

				output.LightDir = float4(normalize(lerp(_WorldSpaceLightPos0.xyz, FragmentToLightSource, _WorldSpaceLightPos0.w)), lerp(1.0, 1.0 / length(FragmentToLightSource), _WorldSpaceLightPos0.w));

				output.pos = UnityObjectToClipPos(input.Vertex);

				output.UV = input.TextCoord;

				return output;
			}

			float4 frag(VertexOutput input) : COLOR
			{
				float nDotl = saturate(dot(input.NormalDir, input.LightDir.xyz));

				float DiffuseCutOff = saturate((max(m_DiffuseThreshold, nDotl) - m_DiffuseThreshold) * 1000);

				float SpecularCutOff = saturate(max(m_Shininess, dot(reflect(-input.LightDir.xyz, input.NormalDir), input.ViewDir)) - m_Shininess) * 1000;

				float OutlineStrength = saturate((dot(input.NormalDir, input.ViewDir) - m_OutlineThickness) * 1000);

				float3 AmbientLight = (1 - DiffuseCutOff) * m_UnlitColor.xyz;

				float3 DiffuseReflection = (1 - SpecularCutOff) * m_Color.xyz * DiffuseCutOff;

				float3 SpecularReflection = m_SpecColor.xyz * SpecularCutOff;

				float3 CombinedLight = (AmbientLight + DiffuseReflection) * OutlineStrength + SpecularReflection;

				return float4(CombinedLight, 1.0);
			}
				ENDCG
		}
	}
		Fallback "Diffuse"
}
