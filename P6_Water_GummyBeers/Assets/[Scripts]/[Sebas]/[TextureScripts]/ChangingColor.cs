﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangingColor : MonoBehaviour
{
    private float m_Speed;
    private Renderer m_Rend;

    void Start()
    {
        m_Speed = Random.Range(0f, 1.5f);

        m_Rend = GetComponent<Renderer>();
    }

    void Update()
    {
        m_Rend.material.SetColor("_Color", HSBColor.ToColor(new HSBColor(Mathf.PingPong(Time.time * m_Speed, 1), 1, 1)));
    }
}
