﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    private Vector3 v3Rotate = Vector3.zero;
    private float min = -50;
    private float max = 50;
    private float rotateSpeed = 45f;


    void Start()
    {
        transform.localEulerAngles = v3Rotate;
    }

    void Update()
    {
        v3Rotate.x += Input.GetAxis("Mouse Y") * rotateSpeed * Time.deltaTime;
        v3Rotate.x = Mathf.Clamp(v3Rotate.x, min, max);
        transform.localEulerAngles = v3Rotate;
    }

    //private float m_MouseSpeed = 100.0f;
    //private float m_ClampAngle = 80.0f;

    //private float m_RotY = 0.0f;
    //private float m_RotX = 0.0f;

    //[SerializeField] private float m_MaxRotationX;
    //[SerializeField] private float m_MaxRotationY;

    //void Start()
    //{
    //    Vector3 rot = transform.localRotation.eulerAngles;
    //    m_RotY = rot.y;
    //    m_RotX = rot.x;


    //}


 
 

    //void Update()
    //{
    //    float mouseX = Input.GetAxis("Mouse X");
    //    float mouseY = -Input.GetAxis("Mouse Y");

    //    m_RotY += mouseX * m_MouseSpeed * Time.deltaTime;
    //    m_RotX += mouseY * m_MouseSpeed * Time.deltaTime;

    //    m_RotX = Mathf.Clamp(m_RotX, -m_ClampAngle, m_ClampAngle);

        

    //    Movement(m_MaxRotationX, m_MaxRotationY);
    //}

    ///// <summary>
    ///// Controlls if the rotation of the player doesn't rotate to far.
    ///// </summary>
    ///// <param name="x">Max rotation on x axis</param>
    ///// <param name="y">Max rotation on y axis</param>
    //private void Movement(float x, float y)
    //{
    //    Quaternion localRotation = Quaternion.Euler(m_RotX, m_RotY, 0.0f);
    //    transform.rotation = localRotation;
    //    //transform.rotation = new Quaternion( Mathf.Clamp(transform.eulerAngles.y, -90, 90));
    //}
}

